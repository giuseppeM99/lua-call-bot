To run this, simply install [MadelineProto](https://docs.madelineproto.xyz/#installation)
If you want, you can use [composer](https://getcomposer.org/download/), just create the `composer.json`

example:
```
{
    "require": {
        "danog/madelineproto": "dev-master"
    },
    "minimum-stability": "dev",
    "repositories": [
        {
            "type": "git",
            "url": "https://github.com/danog/phpseclib"
        }
    ]
}

```
then, run `composer update`.

Then you need to install the php-lua extension
