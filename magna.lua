--[[
  Copywhatever The Doctor (Giuseppe Marino)
  This file is part of the MadelineProto Project
--]]
local serpent = require 'serpent'

local function shuffle(t)
  if type(t) ~= 'table' then
    return t
  end
  local n = {}
  local nt = {}
  while #nt ~= #t do
    local r = math.random(#t)
    if not n[r] then
      table.insert(nt, t[r])
      n[r] = true
    end
  end
  t = nt
  return t
end

local function phparray(array)
  if(type(array) ~= 'table') then return array end
  local narr = {}
  local i = 0
  for _, v in ipairs(array) do
    narr[i] = v
    i = i + 1
  end
  return narr
end

local function luaarray(array)
  if(type(array) ~= 'table') then return array end
  local narr = {}
  local i
  for i = 0, #array do
    table.insert(narr, array[i])
  end
  return narr
end

local function file_exists(name)
  local f = io.open(name, "r")
  if f ~= nil then io.close(f) return true else return false end
end

local songs = require 'songs'

local calls = {}
local users = {}
local times = {}
local programmed_call = (loadfile('programmed_call.lua') or function() return {} end)()

local function serializeProgrammedCalls()
  if programmed_call == nil then
    print("This shit is nil\n")
    return
  end
  local f = io.open('programmed_call.lua', 'w')
  f:write(serpent.dump(programmed_call))
  f:close()
end

local wctxt = [[
Hi, I'm @magnaluna the webradio.
Call _me_ to listen to some **awesome** music, or send /call to make _me_ call _you_ (don't forget to disable call privacy settings!).
You can also program a phone call with /program:
/program 29 August 2018 - call me the 29th of august 2018
/program +1 hour 30 minutes - call me in one hour and thirty minutes
/program next Thursday - call me next Thursday at midnight
Send /start to see this message again.
I also provide advanced stats during calls!
I'm a userbot powered by @MadelineProto, created by @danogentili.
Lua script made by The Doctor.
]]

function rcb(...)
  return ...
end

function rcall(peer)
  local call = _rcall(peer)
  call.configuration.enable_NS = false
  call.configuration.enable_AGC = false
  call.configuration.enable_AEC = false
  call.configuration.shared_config = {
    audio_init_bitrate = 100 * 1000,
    audio_max_bitrate = 120 * 1000,
    audio_min_bitrate = 50 * 1000
  }
  call.parseConfig()
  local otherid = call.getOtherID()
  calls[otherid] = call
  local vis = call.getVisualization() or {}
  times[otherid] = {os.time() or now or 0, messages.sendMessage({peer = call.getOtherID(), message = 'Total running calls: '.. #calls ..'\n\n'..call.getDebugString() .. '\nEmojis: ' .. table.concat(luaarray(vis))})['id']}
  call.playOnHold(phparray(shuffle(songs)))
end

function manageCalls()
  for k, v in pairs(programmed_call) do
    local user, time = v[1], v[2]
    if time < os.time() then
      if not calls[user] then
        rcall(user)
      end
      table.remove(programmed_call, k)
      serializeProgrammedCalls()
    end
  end
  for k, call in pairs(calls) do
    if call.getCallState() == VOIP_CALL_STATE_ENDED then
      times[call.getOtherID()] = nil
      call.__destruct()
      call = nil
      calls[k] = nil
    elseif (times[call.getOtherID()] or {os.time()+10})[1] < os.time() then
      times[call.getOtherID()][1] = os.time() + 10
      local vis = call.getVisualization() or {}
      messages.editMessage({id = times[call.getOtherID()][2], peer = call.getOtherID(), message = 'Total running calls: '.. #calls ..'\n\n'..call.getDebugString() .. '\nEmojis: ' .. table.concat(luaarray(vis))})
    end
  end
end

function madeline_update_callback(data)

  if data._ == 'updateNewMessage' then
    if not (data.message.out or data.message.date < now or not data.message.to_id._ == 'peerUser' or not data.message.from_id) then
      if not users[data.message.from_id] then
        users[data.message.from_id] = true
        messages.sendMessage({peer = data.message.from_id, message = wctxt, parse_mode = 'markdown'})
      end
      if data.message.message == '/start' then
        users[data.message.from_id] = true
        messages.sendMessage({peer = data.message.from_id, message = wctxt, parse_mode = 'markdown'})
      elseif data.message.message == '/debug' then
        messages.sendMessage({peer = data.message.from_id, message = '\nUsers: '.. serpent.block(users, {comment = false}) .. '\nTimes: ' .. serpent.block(times, {comment = false})})
      elseif data.message.message == '/call' then
        rcall(data.message.from_id)
      elseif data.message.message and data.message.message:match('^/program') then
        local time = data.message.message:match('^/program (.*)$')
        local time = strtotime(time)
        if not time or time < os.time() then
          messages.sendMessage({peer = data.message.from_id, message = 'Invalid time provided'})
        else
          messages.sendMessage({peer = data.message.from_id, message = 'OK'})
          table.insert(programmed_call, {data.message.from_id, time})
          serializeProgrammedCalls()
        end
      end
    end
  elseif data._ == 'updatePhoneCall' then
    if data.phone_call and type(data.phone_call) == 'table' and data.phone_call.getCallState and data.phone_call.getCallState() == VOIP_CALL_STATE_INCOMING then
      local call = data.phone_call
      call.configuration.enable_NS = false
      call.configuration.enable_AGC = false
      call.configuration.enable_AEC = false
      call.configuration.shared_config = {
        audio_init_bitrate = 100 * 1000,
        audio_max_bitrate = 120 * 1000,
        audio_min_bitrate = 70 * 1000
      }
      call.parseConfig()
      --[[  if file_exists(string.format('users/%d.raw', call.getOtherID())) then
        call.play(string.format('users/%d.raw', call.getOtherID()))
       else
--]] call.playOnHold(phparray(shuffle(songs)))
      --      end
      if call.accept() then
        calls[call.getOtherID()] = call
        local vis = call.getVisualization() or {}
        times[call.getOtherID()] = {os.time(), messages.sendMessage({peer = call.getOtherID(), message = 'Total running calls: '.. #calls ..'\n\n'..call.getDebugString() .. '\nEmojis: '..table.concat(luaarray(vis))})['id']}
      end
    end
  end
end

VOIP_AUDIO_STATE_NONE = -1
VOIP_AUDIO_STATE_CREATED = 0
VOIP_AUDIO_STATE_CONFIGURED = 1
VOIP_AUDIO_STATE_RUNNING = 2

VOIP_CALL_STATE_NONE = -1
VOIP_CALL_STATE_REQUESTED = 0
VOIP_CALL_STATE_INCOMING = 1
VOIP_CALL_STATE_ACCEPTED = 2
VOIP_CALL_STATE_CONFIRMED = 3
VOIP_CALL_STATE_READY = 4
VOIP_CALL_STATE_ENDED = 5
now = os.time()
